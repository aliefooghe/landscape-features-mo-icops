# Features for Multi-objective Continuous Landscapes (MO-ICOP)

Source code and data for the following paper:
> [Arnaud Liefooghe](https://sites.google.com/site/arnaudliefooghe/), [Sébastien Verel](https://www-lisic.univ-littoral.fr/~verel/), [Benjamin Lacroix](https://rgu-repository.worktribe.com/person/74641/benjamin-lacroix), [Alexandru-Ciprian Zăvoianu](https://rgu-repository.worktribe.com/person/192906/ciprian-zavoianu), [John McCall](https://www3.rgu.ac.uk/dmstaff/mccall-john). **Landscape Features for Multi-objective Interpolated Continuous Optimization Problems**, Genetic and Evolutionary Computation Conference ([GECCO 2021](https://gecco-2021.sigevo.org/)), Lille, France, 2021\
> https://doi.org/10.1145/3449639.3459353


### Contents

* `sampling.R`: provedure to sample solutions in the variable space
* `features.R`: code to compute landscape features
* `features.csv`: data for landscape features
* `sampling-var`: sampled solutions for each problem dimension (common for all problems of size d)
* `sampling-obj`: corresponding objective values for each problem instance
* `paper.pdf`: authored-version of the paper

### List of problem-dependent and landscape features for MO-ICOPs

#### Problem-dependent Features

| name       | description                        |
|------------|------------------------------------|
| d          | number of variables                |
| k          | power of interpolation             |
| seed_n     | proportional number of seeds       |
| nd_seed_n  | proportion of non-dominated seeds  |
| dom_seed_n | proportion of dominated seeds      |


#### Global Landscape Features

| name          | description                                                              |
|---------------|--------------------------------------------------------------------------|
| f_cor         | correlation among objective values                                       |
| dist_x_avg    | average distance among solutions in the variable space                   |
| dist_x_max    | maximum distance among solutions in the variable space                   |
| dist_f_avg    | average distance among solutions in the objective space                  |
| dist_f_max    | maximum distance among solutions in the objective space                  |
| nd_n          | proportion of non-dominated solutions                                    |
| supp_n        | proportion of supported non-dominated solutions                          |
| hv            | hypervolume of non-dominated solutions                                   |
| dist_x_nd_avg | average distance among non-dominated solutions in the variable space     |
| dist_x_nd_max | maximum distance among non-dominated solutions in the variable space     |
| fdc           | fitness-distance correlation among non-dominated solutions               |
| rank_avg      | average rank w.r.t. non-dominated sorting                                |
| rank_max      | maximum rank w.r.t. non-dominated sorting                                |
| rank_ent      | entropy of the number of solutions per rank w.r.t. non-dominated sorting |

#### Features characterising the Landscape Multimodality

| name          | description                                                                   |
|---------------|-------------------------------------------------------------------------------|
| slo_n        | proportion of single-objective local optima per objective                      |
| slo_dist_avg | average distance among single-objective local optima in the variable space     |
| slo_dist_max | maximum distance among single-objective local optima in the variable space     |
| plo_n        | proportion of Pareto local optima                                              |
| plo_dist_avg | average distance among Pareto local optima in the variable space               |
| plo_dist_max | maximum distance among Pareto local optima in the variable space               |
| nd_per_plo   | proportion of non-dominated solutions per Pareto local optimum (nd_n / plo_n)  |
| length_aws   | average length of adaptive walks                                               |
| eval_aws     | average number of calls to the evaluation function performed by adaptive walks |

#### Features characterising the Landscape Evolvability

| name                   | description                                                                   |
|------------------------|-------------------------------------------------------------------------------|
| sup_avg_neig           | average proportion of dominating neighbours                                   |
| inf_avg_neig           | average proportion of dominated neighbours                                    |
| inc_avg_neig           | average proportion of incomparable neighbours                                 |
| lnd_avg_neig           | average proportion of locally non-dominated neighbours                        |
| lsupp_avg_neig         | average proportion of supported locally non-dominated neighbours              |
| dist_x_avg_neig        | average distance from neighbours in the variable space                        |
| dist_f_avg_neig        | average distance from neighbours in the objective space                       |
| dist_f_dist_x_avg_neig | ratio of the average distance from neighbours in the objective and variable spaces (dist_f_avg_neig / dist_x_avg_neig) |
| diff_f_avg_neig        | average difference per objective with neighbours                              |   
| diff_f_dist_x_avg_neig | ratio of the average diff. per objective with neighbours and the dist. in the variable space (diff_f_avg_neig / dist_x_avg_neig)                                                    |
| hv_avg_neig            | average (single) solution's hypervolume                                       |
| hvd_avg_neig           | average (single) solution's hypervolume difference with neighbours            |
| nhv_avg_neig           | average hypervolume from the whole neighbourhood                              |

#### Features characterising the Landscape Ruggdness
| name                   | description                                                                   |
|------------------------|-------------------------------------------------------------------------------|
| sup_cor_neig           | neighbour's correlation of the proportion of dominating neighbours            |
| inf_cor_neig           | neighbour's correlation of the proportion of dominated neighbours             |
| inc_cor_neig           | neighbour's correlation of the proportion of incomparable neighbours          |
| lnd_cor_neig           | neighbour's correlation of the proportion of locally non-dominated neighbours |
| lsupp_cor_neig         | neighbour's correlation of the supported locally non-dominated neighbours     |
| dist_x_cor_neig        | neighbour's correlation of the average distance from neighbours in the variable space |       
| dist_f_cor_neig        | neighbour's correlation of the average distance from neighbours in the objective space |     
| dist_f_dist_x_cor_neig | neighbour's correlation of ratio of the average distance from neighbours in the objective and variable spaces |
| diff_f_cor_neig        | neighbour's correlation of the average difference per objective from neighbours |
| diff_f_dist_x_cor_neig | neighbour's correlation of the ratio of the avg. diff. per obj. from neighbours and the dist. in the variable space |
| hv_cor_neig            | neighbour's correlation of the average (single) solution's hypervolume         |
| hvd_cor_neig           | neighbour's correlation of the average (single) solution's hypervolume difference with neighbours |
| nhv_cor_neig           | neighbour's correlation of the average hypervolume from the whole neighbourhood |
